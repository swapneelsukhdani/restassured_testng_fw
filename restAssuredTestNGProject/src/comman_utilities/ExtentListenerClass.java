package comman_utilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass implements ITestListener{
	
	 
	
	 ExtentSparkReporter sparkreporter;	
	 ExtentReports extentReport;
	 ExtentTest test;
	 
	 
	 
	 public void reportConfigurations()
	 {
	   
		 sparkreporter = new ExtentSparkReporter("./extent-report/report.html");
		 
		 extentReport = new ExtentReports();
		 
		 
		 
		 extentReport.attachReporter(sparkreporter);
		 
		 extentReport.setSystemInfo("OS","Windows 10");
		 extentReport.setSystemInfo("user","Swapneel");
		 
		 
		 sparkreporter.config().setDocumentTitle("extent_report");
		 sparkreporter.config().setReportName("testNG_Report");
		 sparkreporter.config().setTheme(Theme.DARK);

	 }
	 
	 
	 
	 
	 // this method get invoked whenever any of the test cases Execution invoked only once.
	 public void onStart(ITestContext result)
	 {
		 reportConfigurations();
		 System.out.println("on start method invoked .....");
	 }
	 
	 
	 // this method get invoked Before start any of the test cases Execution.
	 public void onFinish(ITestContext result)
	 {
		 System.out.println("on Finish method invoked .....");
		 extentReport.flush();
	 }
	 
	 
	 
	 // this method get invoked whenever any of the test cases Fails.
	 public void onTestFailure(ITestResult result)
	 {
		 System.out.println("name of Test Method Failed ....."+ result.getName());
		 test = extentReport.createTest(result.getName());
		 test.log(Status.FAIL, MarkupHelper.createLabel("Name of the Failed Test Cases is.."+result.getName(), ExtentColor.RED));
	 }
	 
	 
	 // this method get invoked whenever any of the test cases Skipped.
	 public void onTestSkipped(ITestResult result)
	 {
		 System.out.println("Name of Test Method Skipped ....."+ result.getName());
		 test = extentReport.createTest(result.getName());
		 test.log(Status.SKIP, MarkupHelper.createLabel("Name of the Skipped Test Cases is.."+result.getName(), ExtentColor.YELLOW));
	 }
	 
	 
	 // this method get invoked on execution of test cases.(Same as @Before test Annotation)
	 public void onTestStart(ITestResult result)
	 {
		 System.out.println("Name of Test Method Start ....."+ result.getName());
	 }
	 
	 // this method get invoked whenever any of the test case pass.
	 public void onTestSuccess(ITestResult result)
	 {
		 System.out.println("on start method invoked ....."+ result.getName());
		 test = extentReport.createTest(result.getName());
		 test.log(Status.PASS, MarkupHelper.createLabel("Name of the passed Test Cases is.."+result.getName(), ExtentColor.GREEN));
	 }
	 
	 
	 public void onTestFailureButWithinSuccessPercentage(ITestResult result)
	 {
		 
	 }
    
}
