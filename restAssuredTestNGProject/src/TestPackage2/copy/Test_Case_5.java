package TestPackage2.copy;

import java.io.File;
import java.io.IOException;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Environment;
import Repository.RequestBody;
//import Runner.Post_runner;
//import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_5 extends RequestBody {
	@Test
	public static void executor() throws ClassNotFoundException, IOException {
		
		File dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		String requestBody = RequestBody.req_post_tc("Post_TC5");
		String Endpoint = Environment.Hostname() + Environment.Resource_login();
		int statuscode=0;

		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(),
					requestBody, Endpoint);

			statuscode = response.statusCode();

			if (statuscode == 200) {
				Utility.evidenceFileCreator(Utility.testLogName(tstlst.Testname()), dir_name, Endpoint, requestBody,
						response.getHeader("Date"), response.getBody().asString());
				validator(response, requestBody);
				break;
			}
			else {
				System.out.println("Expected status code is not found in current iteration :" +i+ " hence retrying");
			}

		}
		
		if (statuscode!=200) {
			System.out.println("Expected status code not found even after 5 retries hence failing the test case");
			Assert.assertEquals(statuscode, 200);
		}

	}

	public static void validator(Response response, String requestBody) {
		ResponseBody<?> res_body = response.getBody();
		String res_token = res_body.jsonPath().getString("token");
		//Assertions
		Assert.assertNotNull(res_token);

	}

}

