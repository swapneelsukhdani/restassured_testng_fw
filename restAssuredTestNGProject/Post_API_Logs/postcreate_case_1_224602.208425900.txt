Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "jayesh",
    "job": "qa"
}

Response header date is : 
Thu, 07 Mar 2024 17:16:01 GMT

Response body is : 
{"name":"jayesh","job":"qa","id":"521","createdAt":"2024-03-07T17:16:01.807Z"}