Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Mon, 04 Mar 2024 08:30:24 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"196","createdAt":"2024-03-04T08:30:24.505Z"}