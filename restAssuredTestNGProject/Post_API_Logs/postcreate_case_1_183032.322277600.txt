Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 01 Mar 2024 13:00:32 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"268","createdAt":"2024-03-01T13:00:32.454Z"}