Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 19:05:21 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"341","createdAt":"2024-03-07T19:05:21.691Z"}