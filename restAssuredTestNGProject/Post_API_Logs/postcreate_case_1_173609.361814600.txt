Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "vinayak",
    "job": "qa"
}

Response header date is : 
Fri, 08 Mar 2024 12:06:09 GMT

Response body is : 
{"name":"vinayak","job":"qa","id":"475","createdAt":"2024-03-08T12:06:09.272Z"}