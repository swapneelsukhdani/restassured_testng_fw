Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "vinayak",
    "job": "qa"
}

Response header date is : 
Fri, 01 Mar 2024 13:00:34 GMT

Response body is : 
{"name":"vinayak","job":"qa","id":"681","createdAt":"2024-03-01T13:00:34.560Z"}