Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "vinayak",
    "job": "qa"
}

Response header date is : 
Mon, 04 Mar 2024 08:22:27 GMT

Response body is : 
{"name":"vinayak","job":"qa","id":"892","createdAt":"2024-03-04T08:22:27.354Z"}