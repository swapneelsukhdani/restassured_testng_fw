Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vaishu",
    "job": "AutoQA"
}

Response header date is : 
Mon, 04 Mar 2024 08:49:00 GMT

Response body is : 
{"name":"Vaishu","job":"AutoQA","id":"366","createdAt":"2024-03-04T08:49:00.704Z"}