Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "jayesh",
    "job": "qa"
}

Response header date is : 
Fri, 08 Mar 2024 11:02:03 GMT

Response body is : 
{"name":"jayesh","job":"qa","id":"216","createdAt":"2024-03-08T11:02:03.069Z"}