Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vaishu",
    "job": "AutoQA"
}

Response header date is : 
Wed, 06 Mar 2024 18:56:31 GMT

Response body is : 
{"name":"Vaishu","job":"AutoQA","id":"240","createdAt":"2024-03-06T18:56:31.441Z"}