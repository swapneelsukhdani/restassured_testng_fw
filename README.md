# restAssured_testNG_FW




## Getting started

RestAssured is a popular Java library used for testing RESTful APIs, and TestNG is a testing framework for Java. Combining RestAssured with TestNG allows you to create robust and organized API testing frameworks.


@test annotation is used to define method to use any application

@BeforeTest:

Annotations used in TestNG to denote setup methods that run before test suites, test cases, test classes, or test methods, respectively.

@Aftertest:

Annotations used in TestNG to denote teardown methods that run after test suites, test cases, test classes, or test methods, respectively.

@DataProvider:

Used in TestNG to specify a method that supplies data to a test method.
Enables data-driven testing by providing multiple sets of test data to a test method.

Allure Report generation and displayed on browser.

CONFIGURE AND GENERATE EXTENT report.

adding file to gitlab using gitbash

